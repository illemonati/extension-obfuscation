# extension-obfuscation

use \u202E right to left override to hide true file extensions
use unicode look alike characters to avoid windows defender

link to zip
https://files.tong.icu/media/wierdpdf/

## methodology tldr tldr

so you dont fall for this scam in the wild

in windows explorer, assume you have show extensions on and everything

abc.txt is displayed as abc.txt
abc.txt.pdf is displayed as abc.txt.pdf

however in unicode u can use one of the many right to left override control characters
one example is \u202E

so

\u202Eabc would be displayed as cba

abc\u202Edef would be displayed as abcfed

abc\u202Etxt.pdf would be displayed as abcfdp.txt

so it seems file extension would be .txt even thou its a pdf file


this has been used in the wild so windows defender does pick up on it
however
if instead of using abc\u202Etxt.pdf 
u do abc\u202Etхt.pdf

where tхt is not txt but rather other unicode characters that look alike to it, in this case 
its t\u0445t

then windows defender doesnt pick up on it cause as anti virus software is mostly a scam

ofcourse any extension can be used i just used txt and pdf as an example