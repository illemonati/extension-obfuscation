import shutil
import sys
import os


def main():

    if len(sys.argv) < 3:
        print(f"usage: {sys.argv[0]} <input_path> <new_ext>")
        return

    filepath, new_extension = sys.argv[1:3]

    dirname = os.path.dirname(filepath)
    filename, original_ext = os.path.splitext(os.path.basename(filepath))

    print("parent dir: " + dirname)
    print("filename: " + filename)
    print("original_ext: " + original_ext)

    new_file_name_with_ext = f"{filename}\u202E{new_extension[::-1]}{original_ext}"

    shutil.copy2(filepath, new_file_name_with_ext)


if __name__ == '__main__':
    main()
